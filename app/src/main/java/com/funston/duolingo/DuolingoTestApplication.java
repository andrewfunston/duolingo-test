package com.funston.duolingo;

import android.app.Application;

import com.google.gson.Gson;

import junit.framework.Assert;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class DuolingoTestApplication extends Application {

    /** Simple static client for a simple application! */
    public static OkHttpClient sOkHttpClient;
    /** Simple static gson instance for a simple application */
    public static Gson sGson;

    @Override
    public void onCreate() {
        super.onCreate();

        // Proguard removes all assertions and logging statements for prod
        Assert.assertTrue(BuildConfig.DEBUG);

        sGson = new Gson();

        sOkHttpClient = new OkHttpClient()
            .newBuilder()
            .readTimeout(40, TimeUnit.SECONDS)
            .writeTimeout(40, TimeUnit.SECONDS)
            .cache(new Cache(this.getCacheDir(), (1024 * 20)))
            .build();
    }
}
