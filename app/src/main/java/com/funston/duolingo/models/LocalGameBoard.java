package com.funston.duolingo.models;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.funston.duolingo.BuildConfig;
import com.funston.duolingo.util.Pair;
import com.funston.duolingo.util.Point;

import junit.framework.Assert;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;


/**
 * A container for easier business logic manipulation, {@link NetGameBoard} is
 * configured for easiest translation from the badly formatted json provided by the server.
 */
public class LocalGameBoard implements Serializable {

    private static final long serialVersionUID = -6885570166590565618L;

    /** The language the {@code word} is in */
    public final String source_language;
    /** The language the translation and answers are in */
    public final String target_language;
    /** The word to find in the {@code source_language} */
    public final String word;
    /** The character grid the translated words are hidden in */
    public final String[][] character_grid;
    /** The list of solution words and xy coordinates */
    public final ArrayList<Pair<String, ArrayList<Point>>> translationPointListPair;

    /**
     * For building a {@code LocalGameBoard} from a {@link NetGameBoard}.
     *
     * @param netGameBoard A non null {@link NetGameBoard} to build this from.
     */
    public LocalGameBoard(@NonNull NetGameBoard netGameBoard) {
        Assert.assertNotNull(netGameBoard);

        this.source_language = netGameBoard.source_language;
        this.target_language = netGameBoard.target_language;
        this.word = netGameBoard.word;
        this.character_grid = netGameBoard.character_grid;

        this.translationPointListPair = parsePointList(netGameBoard.word_locations);
    }

    @Override
    public String toString() {
        return "LocalGameBoard{" +
            "source_language='" + source_language + '\'' +
            ", target_language='" + target_language + '\'' +
            ", word='" + word + '\'' +
            ", character_grid=" + Arrays.toString(character_grid) +
            ", translationPointListPair=" + translationPointListPair +
            '}';
    }

    /**
     * This should only ever be called on debug builds {@link BuildConfig#DEBUG}. For fast failing during development on
     * bad data. BuildConfig is generated with static members, meaning this will be optimized out if the build is
     * production.
     *
     * @param gameBoard
     * @return so this can be wrapped in an Assert and optimized out.
     */
    public static boolean AssertBoardIsValid(final LocalGameBoard gameBoard) {
        if (BuildConfig.DEBUG) {
            Assert.assertNotNull(gameBoard);

            Assert.assertFalse(TextUtils.isEmpty(gameBoard.source_language));
            Assert.assertFalse(TextUtils.isEmpty(gameBoard.target_language));
            Assert.assertFalse(TextUtils.isEmpty(gameBoard.word));

            // Check the grid, all rows same length, all buckets are length 1
            Assert.assertTrue(gameBoard.character_grid.length > 0);
            Assert.assertTrue(gameBoard.character_grid[0].length > 0);
            final int yLength = gameBoard.character_grid.length;
            for (int x = 1; x < gameBoard.character_grid.length; x++) {
                Assert.assertEquals(yLength, gameBoard.character_grid[x].length);
                for (int y = 0; y < gameBoard.character_grid[x].length; y++) {
                    final String string = gameBoard.character_grid[x][y];
                    Assert.assertNotNull(string);
                    Assert.assertEquals(string.length(), 1);
                }
            }

            // Check all solutions are not null, same number of points as characters in solution
            for (Pair<String, ArrayList<Point>> solutionPair : gameBoard.translationPointListPair) {
                Assert.assertFalse(TextUtils.isEmpty(solutionPair.mLeft));
                Assert.assertEquals(solutionPair.mLeft.length(), solutionPair.mRight.size());
            }
        }

        // So we can wrap this in an Assert.
        return true;
    }

    /**
     * A helper method to convert from {@link NetGameBoard}s and {@link LocalGameBoard}.
     *
     * @param worldLocations
     * @return
     */
    public static ArrayList<Pair<String, ArrayList<Point>>> parsePointList(Map<String, String> worldLocations) {
        final ArrayList<Pair<String, ArrayList<Point>>> retVal = new ArrayList<>();

        for (Map.Entry<String, String> entry : worldLocations.entrySet()) {
            final ArrayList<Point> pointList = new ArrayList<>();
            final String[] xySplit = entry.getKey().split(",");

            boolean isX = true;
            int index = 0;
            for (String xy : xySplit) {
                final int i = Integer.parseInt(xy);

                if (isX) {
                    pointList.add(index, new Point(i, -1));
                    isX = false;
                } else {
                    final Point point = pointList.get(index);
                    point.mY = i;

                    isX = true;
                    index++;
                }
            }

            retVal.add(new Pair<>(entry.getValue(), pointList));
        }

        return retVal;
    }
}
