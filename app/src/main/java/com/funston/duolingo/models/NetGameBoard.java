package com.funston.duolingo.models;

import java.util.Arrays;
import java.util.Map;

/**
 * Eases translation from the provided json, all business logic should instead work with {@link LocalGameBoard}s.
 */
public class NetGameBoard {

    public final String source_language;
    public final String target_language;
    public final String word;
    public final String[][] character_grid;
    public final Map<String, String> word_locations;

    public NetGameBoard() {
        source_language = null;
        target_language = null;
        word = null;
        character_grid = null;
        word_locations = null;
    }

    @Override
    public String toString() {
        return "GameBoard{" +
            "source_language='" + source_language + '\'' +
            ", target_language='" + target_language + '\'' +
            ", word='" + word + '\'' +
            ", character_grid=" + Arrays.toString(character_grid) +
            ", word_locations=" + word_locations +
            '}';
    }

    /* Json format */

    public static final String SOURCE_LANGUAGE_ID = "source_language";
    public static final String TARGET_LANGUAGE_ID = "target_language";
    public static final String WORD_ID = "word";
    public static final String CHARACTER_GRID_ID = "character_grid";
    public static final String WORD_LOCATIONS_ID = "word_locations";
}
