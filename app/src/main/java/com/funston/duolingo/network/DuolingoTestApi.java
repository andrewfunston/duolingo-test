package com.funston.duolingo.network;


import retrofit2.http.GET;
import rx.Observable;

public interface DuolingoTestApi {

    @GET("/duolingo-data/s3/js2/find_challenges.txt")
    Observable<String> getTestDataSet();
}
