package com.funston.duolingo.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.funston.duolingo.R;
import com.funston.duolingo.models.LocalGameBoard;
import com.funston.duolingo.models.NetGameBoard;
import com.funston.duolingo.ui.activity.base.DuoActivity;

import junit.framework.Assert;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

import static com.funston.duolingo.DuolingoTestApplication.sGson;
import static com.funston.duolingo.DuolingoTestApplication.sOkHttpClient;

/**
 * Checks internet connectivity and attempts to load the game boards. Since this is a
 * test the cached version of the endpoint will be used if we don't have connectivity but
 * have stored the endpoint data
 */
public class LoadActivity extends DuoActivity implements Callback {

    public static void start(WinActivity activity) {
        Assert.assertNotNull(activity);

        final Intent intent = new Intent(activity, LoadActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    /** Holder for loaded game list, should only be accessed/modified on the Main/Ui thread */
    ArrayList<LocalGameBoard> mGameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        duoSetContentView(R.layout.activity_load);

        final Request.Builder builder = new Request.Builder();
        final Request request = builder
            .get()
            .url("https://s3.amazonaws.com/duolingo-data/s3/js2/find_challenges.txt")
            .build();

        sOkHttpClient.newCall(request).enqueue(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        tryToGotoNext();
    }

    protected void tryToGotoNext() {
        if (mIsResumed && mGameList != null) {
            MainActivity.start(this, mGameList);
        }
    }

    /* OkHttp Callback */

    @Override
    public void onFailure(Call call, IOException e) {
        Toast.makeText(this, R.string.sorry, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        final String originalBody = response.body().string();
        final String[] split = originalBody.split("\n");

        mGameList = new ArrayList<>();
        for (String splitString : split) {
            final NetGameBoard gameBoard = sGson.fromJson(splitString, NetGameBoard.class);
            mGameList.add(new LocalGameBoard(gameBoard));
        }

        tryToGotoNext();
    }
}
