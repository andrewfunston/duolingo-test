package com.funston.duolingo.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.funston.duolingo.DuolingoTestApplication;
import com.funston.duolingo.R;
import com.funston.duolingo.models.LocalGameBoard;
import com.funston.duolingo.ui.activity.base.DuoActivity;
import com.funston.duolingo.ui.view.BoardGameView;

import junit.framework.Assert;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * The activity that contains and display the board collections
 */
public class MainActivity extends DuoActivity implements BoardGameView.SuccessListener {

    public static final String GAMEBOARD_EXTRA = "com.funston.duolingo.ui.GAMEBOARD_EXTRA";

    /**
     * Starts the MainActivity, should only be started from {@link LoadActivity}. Displays, in order supplied, a
     * collection of {@link LocalGameBoard}
     *
     * @param loadActivity   Non-null activity to start this activity from
     * @param localGameBoard Non-null, non-empty game list to show
     */
    public static void start(@NonNull LoadActivity loadActivity, @NonNull ArrayList<LocalGameBoard> localGameBoard) {
        Assert.assertNotNull(loadActivity);
        Assert.assertNotNull(localGameBoard);
        Assert.assertFalse(localGameBoard.isEmpty());

        final Intent intent = new Intent(loadActivity, MainActivity.class);

        Bundle bundle = new Bundle();
        bundle.putSerializable(GAMEBOARD_EXTRA, localGameBoard);
        intent.putExtras(bundle);

        loadActivity.startActivity(intent);
        loadActivity.finish();
    }

    /* Instance */

    /** The current index board we're displaying */
    int mCurrentBoard = 0;
    /** Our collection of gameboards */
    ArrayList<LocalGameBoard> mLocalGameBoards;
    /** The Currently displayed game board */
    BoardGameView mBoardGameView;

    @Bind(R.id.container) FrameLayout mContainer;
    @Bind(R.id.source_word) TextView mSourceWord;
    @Bind(R.id.destination_language) TextView mSourceLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        duoSetContentView(R.layout.activity_main);

        final Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(GAMEBOARD_EXTRA)) {
            final Serializable serializable = extras.getSerializable(GAMEBOARD_EXTRA);
            if (serializable != null && serializable instanceof ArrayList<?>) {
                mLocalGameBoards = (ArrayList<LocalGameBoard>) serializable;
            }
        }

        if (mLocalGameBoards == null || mLocalGameBoards.isEmpty() || !pSwitchBoard()) {
            Toast.makeText(this, R.string.sorry, Toast.LENGTH_LONG).show();
            try {
                DuolingoTestApplication.sOkHttpClient.cache().evictAll();
            } catch (IOException e) { }
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (!previousBoard()) { super.onBackPressed(); }
    }

    @OnClick(R.id.hint_button)
    public void hintButtonPressed() {
        if (mBoardGameView != null) {
            mBoardGameView.showHint();
        }
    }

    /* BoardGameView.SuccessListener */

    @Override
    public void boardSolved() {
        if (!nextBoard()) { WinActivity.start(this); }
    }

    /* Helper */

    /**
     * Attempts to move back one board.
     *
     * @return true if we swapped, false if we're past the end of the list
     */
    protected boolean nextBoard() {
        mCurrentBoard++;

        return pSwitchBoard();
    }

    /**
     * Attempts to move back one board.
     *
     * @return true if we swapped, false if we're past the beginning of the list
     */
    protected boolean previousBoard() {
        mCurrentBoard--;

        return pSwitchBoard();
    }

    private boolean pSwitchBoard() {
        boolean retVal = false;

        if (mCurrentBoard >= 0 && mCurrentBoard < mLocalGameBoards.size()) {
            if (mBoardGameView != null) {
                mContainer.removeView(mBoardGameView);
            }
            final LocalGameBoard localGameBoard = mLocalGameBoards.get(mCurrentBoard);

            mSourceLanguage.setText(new Locale(localGameBoard.target_language).getDisplayLanguage());
            mSourceWord.setText(localGameBoard.word);

            mBoardGameView = new BoardGameView(this, localGameBoard, this);
            mBoardGameView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mContainer.addView(mBoardGameView);
            mContainer.invalidate();

            retVal = true;
        }

        return retVal;
    }
}
