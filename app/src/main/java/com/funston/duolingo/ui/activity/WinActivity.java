package com.funston.duolingo.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.funston.duolingo.R;
import com.funston.duolingo.ui.activity.base.DuoActivity;

import junit.framework.Assert;

import butterknife.OnClick;

/**
 * Simple win splash screen activity.
 */
public class WinActivity extends DuoActivity {

    public static void start(@NonNull MainActivity activity) {
        Assert.assertNotNull(activity);

        final Intent intent = new Intent(activity, WinActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        duoSetContentView(R.layout.activity_win);
    }

    @OnClick(R.id.replay_button)
    public void clickReplay() {
        LoadActivity.start(this);
    }

    @OnClick(R.id.exit_button)
    public void clickExit() {
        finish();
    }
}
