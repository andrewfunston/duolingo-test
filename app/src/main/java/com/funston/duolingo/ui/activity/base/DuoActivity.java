package com.funston.duolingo.ui.activity.base;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Base class for Android {@link android.app.Activity}, simple class embodies
 * helper functions and useful state
 */
public abstract class DuoActivity extends AppCompatActivity {

    /** Is this activity currently resumed */
    protected boolean mIsResumed = false;

    @Override
    protected void onResume() {
        super.onResume();
        mIsResumed = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsResumed = true;
    }

    /**
     * Sets the content view and then binds the view using {@link ButterKnife#bind(Activity)}
     *
     * @param layout A layout R id
     */
    protected void duoSetContentView(int layout) {
        setContentView(layout);
        ButterKnife.bind(this);
    }
}
