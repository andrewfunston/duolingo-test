package com.funston.duolingo.ui.view;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.funston.duolingo.R;
import com.funston.duolingo.models.LocalGameBoard;
import com.funston.duolingo.util.LineUtil;
import com.funston.duolingo.util.Point;
import com.google.common.base.Objects;

import junit.framework.Assert;

import java.util.ArrayList;

import static com.funston.duolingo.util.LineUtil.DrawLine;

/**
 * Displays, handles gestures, and controls "victory" logic and state for a {@link LocalGameBoard}.
 */
public class BoardGameView extends LinearLayout implements LineUtil.LineConsumer {

    /** The listener we notify when the user completes the current puzzle */
    final SuccessListener mSuccessListener;
    /** A handler attached to the main thread */
    final Handler mHandler = new Handler();
    /** The max X value of the game board */
    final int mX;
    /** The max Y value of the game board */
    final int mY;
    /** The color to display when a tile is "idle," not selected or touched */
    final int mIdleColor;
    /** The color to display when a tile is "touched" */
    final int mSelectedColor;
    /** The color to display when a tile has been selected as part of a complete word */
    final int mWinColor;
    /** Currently pressed list, as in the user's current selection */
    final ArrayList<Pair<Button, Point>> mPressed;
    /** Words the user has already found in the puzzle */
    final ArrayList<String> mFoundWordList = new ArrayList<>(2);
    /** The {@link LocalGameBoard} this View embodies */
    final LocalGameBoard mLocalGameBoard;
    /** All the buttons in grid */
    final Button[][] mButtons;
    /** The touch fields for the buttons, this is a smaller region than the whole button */
    final Rect[][] mTouchFields;
    /** The color the button defaults to when not touched */
    final int[][] mButtonsStates;

    /** Mutable state, set once in {@link BoardGameView#initializeBoard()}, used for drawing/touch events */
    float mBucketWidth, mBucketHeight;
    /** The first point touched when the user starts a gesture */
    Pair<Button, Point> mStartPoint;
    /** The current end point for a users gesture */
    Pair<Button, Point> mCurEndPoint;
    /** Has the user found all words in the grid? */
    boolean mBoardCompleted = false;

    /* View Lifecycle */

    /**
     * Constructor MUST be called on the main thread, a simple game board.
     *
     * @param context
     * @param localGameBoard
     * @param successListener
     */
    public BoardGameView(@NonNull Context context, @NonNull LocalGameBoard localGameBoard, @NonNull SuccessListener successListener) {
        super(context);

        Assert.assertNotNull(context);
        Assert.assertNotNull(successListener);
        Assert.assertTrue(LocalGameBoard.AssertBoardIsValid(localGameBoard));

        mSuccessListener = successListener;

        mIdleColor = getContext().getResources().getColor(R.color.colorPrimary);
        mSelectedColor = getContext().getResources().getColor(R.color.colorAccent);
        mWinColor = getContext().getResources().getColor(R.color.green);
        final int white = getContext().getResources().getColor(R.color.white);

        setOrientation(VERTICAL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setLayoutDirection(LAYOUT_DIRECTION_LTR);
        }

        final LayoutParams rowParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final LayoutParams viewParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f);

        mLocalGameBoard = localGameBoard;
        mX = localGameBoard.character_grid.length;
        mY = localGameBoard.character_grid[0].length;

        mButtons = new Button[mX][mY];
        mTouchFields = new Rect[mX][mY];
        mButtonsStates = new int[mX][mY];

        int y = 0;
        for (String[] outer : localGameBoard.character_grid) {
            final LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutParams(rowParams);

            int x = 0;
            for (String field : outer) {
                final Button button = new Button(context);

                button.setBackgroundColor(mIdleColor);
                button.setText(field);
                button.setTextColor(white);
                button.setLayoutParams(viewParams);
                linearLayout.addView(button);

                mButtons[x][y] = button;
                mButtonsStates[x][y] = mIdleColor;
                x++;
            }

            addView(linearLayout);
            y++;
        }

        mPressed = new ArrayList<>(3);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mBoardCompleted) {
            // ignore events
        } else if (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_CANCEL) {
            checkSolution();
        }

        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mBoardCompleted) {
            // ignore events
        } else if (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP) {
            checkSolution();
        } else if (initializeBoard()) {
            final float touchX = event.getX();
            final float touchY = event.getY();

            final int bucketX = (int) (touchX / mBucketWidth);
            final int bucketY = (int) (touchY / mBucketHeight);

            if (!(bucketX >= mX || bucketY >= mY)) {
                if (mTouchFields[bucketX][bucketY].contains((int) touchX, (int) touchY)) {
                    final Button button = mButtons[bucketX][bucketY];
                    final Point point = new Point(bucketX, bucketY);

                    processEvent(button, point);
                }
            }
        }

        return true;
    }

    /**
     * Processes a touch event, handles short circuits for a touch event already handled visually.
     *
     * @param pressed
     * @param point
     */
    protected void processEvent(Button pressed, Point point) {
        // First press
        if (mStartPoint == null) {
            mStartPoint = new Pair<>(pressed, point);
            pressed.setBackgroundColor(mSelectedColor);
            mPressed.add(mStartPoint);
        }
        // Still on the start point
        else if (mStartPoint.first == pressed) {
            if (mPressed.size() == 1) {
                // Do nothing
            } else {
                clearBoardExceptStart();
            }
        }
        // Same selection as last time
        else if (mCurEndPoint != null && mCurEndPoint.first == pressed) {
        }
        // Redraw the line
        else {
            Pair<Button, Point> temp = mStartPoint;
            clearBoard();

            mStartPoint = temp;
            mCurEndPoint = new Pair<>(pressed, point);

            DrawLine(
                this,
                mStartPoint.second.mX,
                mStartPoint.second.mY,
                mCurEndPoint.second.mX,
                mCurEndPoint.second.mY);
        }
    }

    /* Hooks */

    /**
     * Must be called on Main/UI thread! Hints an un-selected word.
     */
    public void showHint() {
        // TODO - random point in the word, not first letter

        if (mStartPoint == null) {
            for (final com.funston.duolingo.util.Pair<String, ArrayList<Point>> answerPair : mLocalGameBoard.translationPointListPair) {

                boolean alreadyFound = false;
                for (String found : mFoundWordList) {
                    if (Objects.equal(answerPair.mLeft, found)) { alreadyFound = true; }
                }

                if (!alreadyFound && answerPair.mRight.size() > 0) {
                    final Point point = answerPair.mRight.get(0);
                    final Button button = mButtons[point.mX][point.mY];

                    final ObjectAnimator objectAnimator = ObjectAnimator.ofInt(button, "BackgroundColor", mIdleColor, mSelectedColor, mWinColor, mIdleColor);
                    objectAnimator.setEvaluator(new ArgbEvaluator());
                    objectAnimator.setDuration(900);
                    objectAnimator.start();
                    break;
                }
            }
        }
    }

    /* Helper */

    /**
     * Initializes the boards bounds when the view is ready to be measured
     *
     * @return true if the view was properly initialized, false if the view cannot be measured yet.
     */
    protected boolean initializeBoard() {
        if (mTouchFields[0][0] == null && getWidth() > 0) {

            final float mWidth = getWidth();
            mBucketWidth = mWidth / mX;
            final float mBucketWidthTenth = mBucketWidth * .1f;

            final float mHeight = getHeight();
            mBucketHeight = mHeight / mY;
            final float mBucketHeightTenth = mBucketHeight * .1f;

            for (int y = 0; y < mY; y++) {
                float top = y * mBucketHeight;
                float bottom = top + mBucketHeight;

                for (int x = 0; x < mX; x++) {
                    float left = x * mBucketWidth;
                    float right = left + mBucketWidth;

                    mTouchFields[x][y] = new Rect(
                        ((int) (left + mBucketWidthTenth)),
                        ((int) (top + mBucketHeightTenth)),
                        ((int) (right - mBucketWidthTenth)),
                        ((int) (bottom - mBucketHeightTenth)));
                }
            }
        }

        return mTouchFields[0][0] != null;
    }

    /**
     * At the end of each gesture event, check to see if the user has found a word.
     */
    protected void checkSolution() {
        boolean success = false;
        for (com.funston.duolingo.util.Pair<String, ArrayList<Point>> listPair : mLocalGameBoard.translationPointListPair) {
            if (listPair.mRight.size() == mPressed.size() && !mFoundWordList.contains(listPair.mLeft)) {
                boolean wholeWordFound = true;

                for (Point point : listPair.mRight) {
                    boolean found = false;
                    for (Pair<Button, Point> pressedPair : mPressed) {
                        if (pressedPair.second.equals(point)) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        wholeWordFound = false;
                        break;
                    }
                }

                if (wholeWordFound) {
                    mFoundWordList.add(listPair.mLeft);
                    success = true;
                }
            }
        }

        if (success) {
            for (Pair<Button, Point> pressedPair : mPressed) {
                final Point coord = pressedPair.second;
                mButtonsStates[coord.mX][coord.mY] = mWinColor;
            }
        }

        // They've won!
        if (mFoundWordList.size() == mLocalGameBoard.translationPointListPair.size()) {
            mBoardCompleted = true;
            mHandler.post(new Runnable() {
                @Override
                public void run() { mSuccessListener.boardSolved(); }
            });
        }

        clearBoard();
    }

    /**
     * Clears the board by reseting the color of all clicked buttons EXCEPT the start,
     * the start only needs reset at the end of a touch gesture
     */
    protected void clearBoardExceptStart() { pClearBoard(false); }

    /**
     * Clears the each button on the board to {@code mButtonsStates} color!
     */
    protected void clearBoard() { pClearBoard(true); }

    private void pClearBoard(boolean removeStart) {
        if (removeStart) { mStartPoint = null; }
        mCurEndPoint = null;

        for (Pair<Button, Point> pair : mPressed) {
            if (removeStart || pair.first != mStartPoint.first) {
                pair.first.setBackgroundColor(mButtonsStates[pair.second.mX][pair.second.mY]);
            }
        }

        mPressed.clear();
        if (!removeStart) { mPressed.add(mStartPoint); }
    }

    /* LineUtil.LineConsumer */

    @Override
    public void plot(int x, int y) {
        Assert.assertTrue((x >= 0) && (x < mButtons.length));
        Assert.assertTrue((y >= 0) && (y < mButtons[0].length));

        if ((x >= 0) && (x < mButtons.length) &&
            (y >= 0) && (y < mButtons[0].length)) {
            final Button button = mButtons[x][y];
            button.setBackgroundColor(mSelectedColor);
            mPressed.add(new Pair<>(button, new Point(x, y)));
        }
    }

    /* Inner Classes */

    /**
     * Notifies listeners when the board has been solved.
     */
    public interface SuccessListener {

        /**
         * The board this listener is attached to has been solved.
         * All events will be sent on the UI thread.
         */
        void boardSolved();
    }
}
