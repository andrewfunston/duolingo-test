package com.funston.duolingo.util;

import android.support.annotation.NonNull;

import junit.framework.Assert;

/**
 * Utils for drawing
 */
public class LineUtil {

    /**
     * Draws a line using the Bresenham Line Drawing Algorithm. Implementation a lightly modified version of:
     * <a href="http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#Java">Rossetta Code</a>
     *
     * @param lineConsumer A non-null consumer of the data calculated by the algorithm.
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    public static void DrawLine(@NonNull LineConsumer lineConsumer, int x1, int y1, int x2, int y2) {
        Assert.assertNotNull(lineConsumer);
        if (lineConsumer == null) { return; }

        // delta of exact value and rounded value of the dependant variable
        int d = 0;

        int dy = Math.abs(y2 - y1);
        int dx = Math.abs(x2 - x1);

        int dy2 = (dy << 1); // slope scaling factors to avoid floating
        int dx2 = (dx << 1); // point

        int ix = x1 < x2 ? 1 : -1; // increment direction
        int iy = y1 < y2 ? 1 : -1;

        if (dy <= dx) {
            for (; ; ) {

                lineConsumer.plot(x1, y1);
                if (x1 == x2) { break; }
                x1 += ix;
                d += dy2;
                if (d > dx) {
                    y1 += iy;
                    d -= dx2;
                }
            }
        } else {
            for (; ; ) {
                lineConsumer.plot(x1, y1);
                if (y1 == y2) { break; }
                y1 += iy;
                d += dx2;
                if (d > dy) {
                    x1 += ix;
                    d -= dy2;
                }
            }
        }
    }

    /**
     * A simple consumer for {@link LineUtil#DrawLine(LineConsumer, int, int, int, int)}
     */
    public interface LineConsumer {
        /**
         * A request to plot the x,y coordinate
         *
         * @param x
         * @param y
         */
        void plot(int x, int y);
    }

    private LineUtil() {}
}
