package com.funston.duolingo.util;

import java.io.Serializable;

/**
 * {@link Serializable} Pair class, simple utility class.
 *
 * @param <L> Implements {@link Serializable}
 * @param <R> Implements {@link Serializable}
 */
public class Pair<L extends Serializable, R extends Serializable> implements Serializable {

    private static final long serialVersionUID = -2145599127399990248L;

    public final L mLeft;
    public final R mRight;

    public Pair(final L left, final R right) {
        mLeft = left;
        mRight = right;
    }
}
