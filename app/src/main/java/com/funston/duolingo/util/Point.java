package com.funston.duolingo.util;

import java.io.Serializable;

/**
 * Simple {@link Serializable} Point class. Embodies a mutable x,y coordinate.
 */
public class Point implements Serializable {

    private static final long serialVersionUID = -3090741161912326675L;

    public int mX, mY;

    public Point(int x, int y) {
        mX = x;
        mY = y;
    }

    @Override
    public boolean equals(Object o) {
        boolean retVal = false;
        if (o != null && o instanceof Point) {
            final Point other = (Point) o;
            retVal = other.mX == this.mX && other.mY == this.mY;
        }
        return retVal;
    }

    @Override
    public String toString() {
        return "Point{" +
            "mX=" + mX +
            ", mY=" + mY +
            '}';
    }
}
